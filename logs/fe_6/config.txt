#!/usr/bin/python

"""config.txt: Configuration file. Contains the parameter needed.
"""
__author__      = 'Kuan-yin Chen'
__copyright__   = 'Copyright 2014, NYU-Poly'


# ---- Frequently Used ----
DIR_TOPO = './topologies/geant'
SIM_TIME = 20.0

DO_REROUTE = 0              # Do elephant flow rerouting (please refer to paper draft)


# ----
ROUTING_MODE = 'tablelb'        # Supported routing modes:
                                # 'tablelb': Table load-balancing routing using k-path
                                #               (default to Yen's k-path algorithm)
                                # 'ecmp': Equal-cost multi-path
                                # 'spf': Shortest-path first
K_PATH = 6                      # Number of predefined path per src-dst pair
K_PATH_METHOD = 'yen'           # The algorithm used to set up k-path database

# ---------------------------------------
# Switch/link Initialization Parameters
# ---------------------------------------
OVERRIDE_TABLESIZE = True
TABLESIZE_PER_SW = 400
OVERRIDE_N_HOSTS = True
N_HOSTS_PER_SW = 10
OVERRIDE_CAP = True
CAP_PER_LINK = 1e9


# ----------------------------------------
# Screen Printing Options
# ----------------------------------------
SHOW_PROGRESS = 0
SHOW_EVENTS = 0
SHOW_REJECTS = 0
SHOW_K_PATH_CONSTRUCTION = 0
SHOW_LINK_CONGESTION = 0
SHOW_TABLE_OVERFLOW = 0
SHOW_FLOW_CALC = 0
SHOW_REROUTE = 0

# ----------------------------------------
# Logging Options
# ----------------------------------------
LOG_CONFIG = 1
LOG_LINK_UTIL = 1
LOG_TABLE_UTIL = 1
LOG_FLOW_STATS = 1
LOG_SUMMARY = 1

LOG_DIR = './logs'

# Specified in column order
LOG_FLOW_STATS_FIELDS = ['src_ip', 'dst_ip', 'src_node', 'dst_node', 'flow_size', 'avg_rate', \
                         'arrive_time', 'install_time', 'end_time', 'duration', 'resend', 'reroute']

# ----------------------------------------
# Time-related Parameters
# ----------------------------------------
PERIOD_LOGGING = 0.100          # Period of doing link util and table util logging.

SW_CTRL_DELAY = 0.005           # Switch-to-controller delay
CTRL_SW_DELAY = 0.005           # Controller-to-switch delay
IDLE_TIMEOUT = 1.000            # Idle timeout
REJECT_TIMEOUT = 0.100          # Timeout for flow re-request if rejected due to overflow

PERIOD_REROUTE = 1.000          # Period of rerouting

# ----------------------------------------
# Flow Generation Parameters
# ----------------------------------------
FLOW_DST_MODEL = 'uniform'      # Uniform: Randomly pick one non-src
FLOW_SIZE_MODEL = 'uniform'
FLOW_INTARR_MODEL = 'saturate'  # Saturate: generate another flow immediatedly
                                # after flow ends at a host
FLOW_RATE_MODEL = 'uniform'

# Applicable only to FLOW_SIZE_MODEL='uniform'
FLOW_SIZE_LO = 5e7
FLOW_SIZE_HI = 10e7
FLOW_RATE_LO = 5e6
FLOW_RATE_HI = 10e6

# Applicable only to FLOW_SIZE_MODEL='bimodal'
PROB_LARGE_FLOW = 0.1
FLOW_SIZE_LARGE_LO = 1e8
FLOW_SIZE_LARGE_HI = 5e8
FLOW_SIZE_SMALL_LO = 1e6
FLOW_SIZE_SMALL_HI = 5e6


LATEST_INIT_FLOW_TIME = 1.0     # Latest initial flow arrival time
FLOWGEN_DELAY = 0               # When a host's flow ends, generate a new flow after this delay

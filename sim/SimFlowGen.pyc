ó
Tc           @   sw   d  Z  d Z d Z d d l m Z m Z d d l Z d d l Z	 d d l
 Z d d l Td d l Td d	 d     YZ d S(
   sN   sim/SimFlowGen.py: The SDN controller class, capable of doing k-path routing.
s   Kuan-yin Chens   Copyright 2014, NYU-Polyiÿÿÿÿ(   t   heappusht   heappopN(   t   *t
   SimFlowGenc           B   s   e  Z d  Z d   Z d   Z d   Z d   Z e j e j	 d  Z
 e j e j e j e j d  Z d   Z e j e j d  Z d	   Z d
   Z RS(   sÁ   Flow Generator.

    Attributes:
        hosts (dict): Hosts database. Key: Host IP, Value: attached edge switch.
                      Directly copy-assigned during SimFlowGen.__init__()

    c         C   s   | j  |  _  d  S(   N(   t   hosts(   t   selft   sim_core(    (    s   sim/SimFlowGen.pyt   __init__   s    c         C   sZ   xS t  rU t j d k r* |  j |  } n |  j |  } | | f | j k r Pq q W| S(   s\  Given src_ip, pick a dst_ip using specified random model.

        Args:
            src_ip (netaddr.IPAddress)
            sim_core (instance of SimCore)

        Extra Notes:
            sim_core is passed into this function because we need to check if the
            picked (src_ip, dst_ip) does not overlap with any flow in sim_core.

        t   uniform(   t   Truet   cfgt   FLOW_DST_MODELt   pick_dst_uniformt   flows(   R   t   src_ipR   t   dst_ip(    (    s   sim/SimFlowGen.pyt   pick_dst   s    	c         C   sM   d } x@ t  rH t j |  j j    } |  j | |  j | k s	 Pq	 q	 W| S(   sÜ   Given src_ip, pick a dst_ip using uniform random model.

        Args:
            src_ip (netaddr.IPAddress)

        Extra Notes:
            src_ip and dst_ip will never be under the same edge node (switch).

        i    (   R	   t   rdt   choiceR   t   keys(   R   R   R   (    (    s   sim/SimFlowGen.pyR   7   s    
	c      	   C   s   d } t  j d k r6 |  j d t  j d t  j  } nZ t  j d k rx |  j d t  j d t  j d t  j d	 t  j	  } n |  j t  j t  j  } | S(
   s  Generate flow size according to specified random model.

        Args:

        Return:
            float64: Flow size. Although real-world flow sizes (in bytes) are integers,
                     We cast them to float64 for the sake of compatibility with functions using it.

        g        R   t   lot   hit   bimodalt   large_lot   large_hit   small_lot   small_hi(
   R
   t   FLOW_SIZE_MODELt   get_flow_size_uniformt   FLOW_SIZE_LOt   FLOW_SIZE_HIt   get_flow_size_bimodalt   FLOW_SIZE_LARGE_LOt   FLOW_SIZE_LARGE_HIt   FLOW_SIZE_SMALL_LOt   FLOW_SIZE_SMALL_HI(   R   t   ret(    (    s   sim/SimFlowGen.pyt   get_flow_sizeI   s    
!		c         C   s(   t  j j | |  } t | d  } | S(   sõ   Generate flow size according to uniform random model

        Args:
            low (float64): Lower bound
            high (float64): Upper bound

        Return:
            float64: Uniform random flow size, round to integral digit.

        i    (   t   npt   randomR   t   round(   R   R   R   R$   (    (    s   sim/SimFlowGen.pyR   a   s    c         C   sj   t  j j d d  } d } | t j k  rB t  j j | |  } n t  j j | |  } t | d  } | S(   sD  Generate flow size according to bimodal random model

        Args:
            large_low, large_hi (float64): Lower & upper bound for large flows
            small_low, small_hi (float64): Lower & upper bound for small flows

        Return:
            float64: Uniform random flow size, round to integral digit.

        i    i   g        (   R&   R'   R   R
   t   PROB_LARGE_FLOWR(   (   R   R   R   R   R   t	   roll_diceR$   (    (    s   sim/SimFlowGen.pyR   q   s    c         C   sI   d } t  j d k r0 |  j t  j t  j  } n t t  j t  j  } | S(   s   Generate flow rate according to specified random model.

        Args:

        Return:
            float64: Flow rate (bytes per second).

        g        R   (   R
   t   FLOW_RATE_MODELt   get_flow_rate_uniformt   FLOW_RATE_LOt   FLOW_RATE_HI(   R   R$   (    (    s   sim/SimFlowGen.pyt   get_flow_rate   s
    	c         C   s   t  j j | |  } | S(   sÙ   Generate flow rate according to uniform random model

        Args:
            lo (float64): Lower bound
            hi (float64): Upper bound

        Return:
            float64: Uniform random flow rate.

        (   R&   R'   R   (   R   R   R   R$   (    (    s   sim/SimFlowGen.pyR,      s    c         C   sU   |  j  | |  } |  j   } |  j   } t d | d | d | d | d |  } | S(   Nt   ev_timeR   R   t	   flow_sizet	   flow_rate(   R   R%   R/   t   EvFlowArrival(   R   R0   t   srcR   t   dstt   fsizet   frateR$   (    (    s   sim/SimFlowGen.pyt   gen_new_flow_with_src«   s
    'c         C   sj   t  j d k rf xT |  j D]F } t j j d t  j  } |  j | | |  } t | | | f  q Wn  d S(   sn  When simulation starts, generate a set of initial flows.
        We generate exactly one initial flow for each source host.
        The initial flows, as EvFlowArrival events, will be enqueued to ev_queue

        Args:
            ev_queue (list of instances inherited from SimEvent): Event queue
            sim_core (instance of SimCore): Simulation core
        t   saturateg        N(	   R
   t   FLOW_INTARR_MODELR   R&   R'   R   t   LATEST_INIT_FLOW_TIMER8   R    (   R   t   ev_queueR   t   hstR0   t   event(    (    s   sim/SimFlowGen.pyt   gen_init_flows´   s    	(   t   __name__t
   __module__t   __doc__R   R   R   R%   R
   R   R   R   R    R!   R"   R#   R   R/   R-   R.   R,   R8   R?   (    (    (    s   sim/SimFlowGen.pyR      s   							(    (   RB   t
   __author__t   __copyright__t   heapqR    R   R'   R   t   numpyR&   t   netaddrt   nat   configt   SimEventR   (    (    (    s   sim/SimFlowGen.pyt   <module>   s   

